import React, { useState } from 'react';
import Head from 'next/head'
import { Container, Hr, PhotoGallary } from '../styles/pages/Home'
// import {
//   Collapse,
//   Navbar,
//   NavbarToggler,
//   NavbarBrand,
//   Nav,
//   NavItem,
//   NavLink,
//   UncontrolledDropdown,
//   DropdownToggle,
//   DropdownMenu,
//   DropdownItem,
//   NavbarText
// } from 'reactstrap';


export default function Home() {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);
  return (
    <Container>
      <Head>
        <title>Profile</title>
        {/* <link rel="icon" href="/favicon.ico" /> */}
      </Head>

      <header>
        {/* <Navbar color="light" light expand="md">
          <NavbarBrand href="/">Ashley.</NavbarBrand>
          <NavbarToggler onClick={toggle} />
          <Collapse isOpen={isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink href="/">Home</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="#">My History</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="#gallery">My Gallery</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="#">Follow Me</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="#">Contact Me</NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar> */}

        <nav className="navbar navbar-light navbar-expand-md fixed-top hy-navbar" >
          <div className="container">
            <a className="navbar-brand" href="#">Ashley.</a>
            <button data-toggle="collapse" className="navbar-toggler" data-target="#navcol-1">
              <span className="sr-only">Toggle navigation</span>
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navcol-1">
              <ul className="nav navbar-nav float-right ml-auto">
                <li className="nav-item"><a className="nav-link active" href="#">Home</a></li>
                <li className="nav-item"><a className="nav-link" href="#">My Story</a></li>
                <li className="nav-item"><a className="nav-link" href="#gallery">My Gallery</a></li>
                <li className="nav-item"><a className="nav-link" href="#">Follow Me</a></li>
                <li className="nav-item"><a className="nav-link" href="#">Contact Me</a></li>
              </ul>
            </div>
          </div>
        </nav>
      </header>

      <div className="jumbotron text-center" id="home">
        <h1 className="display-1 text-uppercase">Ashley.</h1>
        <Hr><hr /></Hr>
        <p>Vlogging About Beauty, Fashion, Life Hacks &amp; My Life.</p>
      </div>

      <section id="story">
        <div className="container">
          <div className="row">
            <div className="col">
              <button className="btn btn-link" type="button">
                <img className="img-fluid"
                  src="/assets/img/imagem1.jpg" />
              </button>
            </div>

            <div className="col">
              <h1 className="text-uppercase">This is my story</h1>
              <p>Lorem Ipsum&nbsp;é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e
              vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de
              tipos e os embaralhou para fazer um livro
              de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a
              editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60,
              quando a Letraset lançou decalques contendo
              passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de
                        editoração eletrônica como Aldus PageMaker.</p>
            </div>
          </div>
        </div>
      </section>

      <PhotoGallary id="gallery" className="photo-gallery">
        <div className="container">
          <div className="intro">
            <h2 className="text-center">MyGallery</h2>
            <p className="text-center">
              Nunc luctus in metus eget fringilla. Aliquam sed justo ligula. Vestibulum nibh
              erat, pellentesque ut laoreet vitae.
            </p>
          </div>

          <div className="row photos">
            <div className="col-sm-6 col-md-4 col-lg-3 item">
              <a data-lightbox="photos"
                href="/assets/img/imagem1.jpg">
                <img
                  className="img-fluid"
                  src="/assets/img/imagem1.jpg" />
              </a>
            </div>

            <div className="col-sm-6 col-md-4 col-lg-3 item">
              <a data-lightbox="photos"
                href="/assets/img/MICHELE-e1583529059177-1024x1005.jpg">
                <img className="img-fluid"
                  src="/assets/img/MICHELE-e1583529059177-1024x1005.jpg" />
              </a>
            </div>

            <div className="col-sm-6 col-md-4 col-lg-3 item"><a data-lightbox="photos"
              href="/assets/img/mulher-poderosa-870x470.webp">
              <img className="img-fluid"
                src="/assets/img/mulher-poderosa-870x470.webp" />
            </a>
            </div>

            <div className="col-sm-6 col-md-4 col-lg-3 item"><a data-lightbox="photos"
              href="/assets/img/shutterstock_262414568_1543923286.jpg">
              <img className="img-fluid"
                src="/assets/img/shutterstock_262414568_1543923286.jpg" />
            </a>
            </div>

            <div className="col-sm-6 col-md-4 col-lg-3 item">
              <a data-lightbox="photos"
                href="/assets/img/fashion-3080644_1920-846x400.jpg">
                <img className="img-fluid"
                  src="/assets/img/fashion-3080644_1920-846x400.jpg" />
              </a>
            </div>

            <div className="col-sm-6 col-md-4 col-lg-3 item">
              <a data-lightbox="photos"
                href="/assets/img/kamala-harris-2.jpg">
                <img className="img-fluid"
                  src="/assets/img/kamala-harris-2.jpg" />
              </a>
            </div>
          </div>
        </div>
      </PhotoGallary>

    </Container>
  )
}
