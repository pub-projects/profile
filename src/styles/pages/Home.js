import styled from 'styled-components'

export const Container = styled.div`
  
  .hy-navbar {
    background: rgba(255, 255, 255, 0.8);
  }
`

export const Hr = styled.div`
  display: flex;
  justify-content: center;

  hr {
    width: 300px;
    border-color: #c92728;
    margin-top: 8px;
  }

`
export const PhotoGallary = styled.div`
  .img-fluid {
    height: 100%;
    width: 100%;
    object-fit: cover;
  }

  .photo-gallery {
    color: #313437;
    background-color: #fff;
  }

  .photo-gallery p {
    color: #7d8285;
  }

  .photo-gallery h2 {
    font-weight: bold;
    margin-bottom: 40px;
    padding-top: 40px;
    color: inherit;
  }

  @media (max-width:767px) {
    .photo-gallery h2 {
      margin-bottom: 25px;
      padding-top: 25px;
      font-size: 24px;
    }
  }

  .photo-gallery .intro {
    font-size: 16px;
    max-width: 500px;
    margin: 0 auto 40px;
  }

  .photo-gallery .intro p {
    margin-bottom: 0;
  }

  .photo-gallery .photos {
    padding-bottom: 20px;
  }

  .photo-gallery .item {
    padding-bottom: 30px;
  }
`